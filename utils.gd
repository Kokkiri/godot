extends Node


func set_color(_id, opti, label):
	if opti == _id:
		label.add_color_override('font_color', Color(255, 255, 255))
	else:
		label.add_color_override('font_color', Color(0.517647, 0.517647, 0.517647))

func selector(_id, opti, button):
	if opti == _id:
		button.grab_focus()

func _next(state, options_num, num):
	var t = []
	for i in range(1, options_num + 1):
		t.append(i)
	for i in range(len(t)):
		if t[i] == state:
			return t[(i + num)% len(t)]
