extends Node

var fullscreen = true
var music = -6
var sfx = -6

func _input(event):
	if event.is_action_pressed('toggle_fullscreen'):
		OS.window_fullscreen = !OS.window_fullscreen
		get_tree().set_input_as_handled()
