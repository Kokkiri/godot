extends Node

var res_loader : ResourceInteractiveLoader = null
var loading_thread : Thread = null

signal quit
signal replace_main_scene

onready var menu_bg = $Control/title_screen_bg/menu_bg
onready var start = $Control/title_screen_bg/menu_bg/main_menu/start_button
onready var loading = $Control/title_screen_bg/loading
onready var loading_done_timer = $Control/title_screen_bg/loading/Timer
onready var anim_intro = $Control/title_screen_bg/anim_intro
onready var start_sound = $Control/Sfx/start

func interactive_load(loader):
	while true:
		var status = loader.poll()
		if status == OK:
			continue
		elif status == ERR_FILE_EOF:
			loading_done_timer.start()
			break
		else:
			print("Error while loading level: " + str(status))
			menu_bg.show()
			loading.hide()
			break

func loading_done(loader):
	loading_thread.wait_to_finish()
	emit_signal("replace_main_scene", loader.get_resource())
	res_loader = null
	# Weirdly, "res_loader = null" is needed as otherwise
	# loading the resource again is not possible.

func _on_Timer_timeout():
	loading_done(res_loader)

func _on_start_button_pressed():
#	var anim = preload("res://anim/anim.tscn").instance()
#	add_child(anim)
	menu_bg.hide()
	anim_intro.hide()
	loading.show()
	loading.play('jupiter_transition')
	start_sound.play()
	var path = "res://anim/anim.tscn"
#	actuellement ne rentre pas dans le if
	if ResourceLoader.has_cached(path):
		emit_signal("replace_main_scene", ResourceLoader.load(path))
	else:
		print('load_interactive')
		res_loader = ResourceLoader.load_interactive(path)
		loading_thread = Thread.new()
		#warning-ignore:return_value_discarded
		loading_thread.start(self, "interactive_load", res_loader)
		
