extends Control


onready var menu_bg = $title_screen_bg/menu_bg
onready var menu_opt_num = $title_screen_bg/menu_bg/main_menu.get_child_count()
onready var start = $title_screen_bg/menu_bg/main_menu/start_button
onready var option = $title_screen_bg/menu_bg/main_menu/option_button
onready var quit = $title_screen_bg/menu_bg/main_menu/quit_button
onready var option_bg = $title_screen_bg/option_bg
onready var option_opt_num = $title_screen_bg/option_bg/option_menu.get_child_count()
onready var fullscreen_button = $title_screen_bg/option_bg/option_menu/fullscreen/button
onready var fullscreen_value = $title_screen_bg/option_bg/option_menu/fullscreen/value
onready var music_button = $title_screen_bg/option_bg/option_menu/music/button
onready var music_value = $title_screen_bg/option_bg/option_menu/music/value
onready var sfx_button = $title_screen_bg/option_bg/option_menu/sfx/button
onready var sfx_value = $title_screen_bg/option_bg/option_menu/sfx/value
onready var anim_intro = $title_screen_bg/anim_intro
onready var back = $title_screen_bg/option_bg/option_menu/back
onready var select_sound = $Sfx/select
onready var cancel_sound = $Sfx/cancel
onready var intro_sound = $Sfx/jupiter
onready var option_sound = $Sfx/short_sweep
onready var anim_mask = $title_screen_bg/Menu_mask/AnimationPlayer

var count = 0
var lastTime = 0
var interval = 9
var opt = 1
var is_changing_for_option = false
var slide_speed = 20


func _ready():
	anim_intro.play()
	intro_sound.pitch_scale = 0.3
	intro_sound.play()

func _process(_delta):
	if is_changing_for_option == false:

		if Input.is_action_pressed('ui_up') and (count > lastTime + interval):
			lastTime = count
			opt = Utils._next(opt, menu_opt_num, -1)
			select_sound.play()
		if Input.is_action_pressed('ui_down') and (count > lastTime + interval):
			lastTime = count
			opt = Utils._next(opt, menu_opt_num, 1)
			select_sound.play()

		count += 1
		Utils.selector(1, opt, start)
		Utils.selector(2, opt, option)
		Utils.selector(3, opt, quit)

	else:

		if Input.is_action_pressed('ui_up') and (count > lastTime + interval):
			lastTime = count
			opt = Utils._next(opt, option_opt_num, -1)
			select_sound.play()
		if Input.is_action_pressed('ui_down') and (count > lastTime + interval):
			lastTime = count
			opt = Utils._next(opt, option_opt_num, 1)
			select_sound.play()

		count += 1
		Utils.selector(1, opt, fullscreen_button)
		Utils.set_color(1, opt, fullscreen_value)
		
		Utils.selector(2, opt, music_button)
		Utils.set_color(2, opt, music_value)
		
		Utils.selector(3, opt, sfx_button)
		Utils.set_color(3, opt, sfx_value)
		
		Utils.selector(4, opt, back)

func _on_option_button_pressed():
	opt = 1
	is_changing_for_option = true
	anim_mask.play("reduce-expand")
	yield(get_tree().create_timer(0.3), "timeout")
	option_sound.play()	
	menu_bg.hide()
	option_bg.show()	
	fullscreen_button.grab_focus()

func _on_back_pressed():
	opt = 1
	is_changing_for_option = false
	anim_mask.play("reduce-expand")
	cancel_sound.play()	
	yield(get_tree().create_timer(0.3), "timeout")	
	option_bg.hide()
	menu_bg.show()
	start.grab_focus()

func _on_quit_button_pressed():
	cancel_sound.play()
	yield(get_tree().create_timer(0.5), "timeout")
	get_tree().quit()
