extends VBoxContainer

onready var fullscreen_button = $fullscreen/button
onready var fullscreen_value = $fullscreen/value
onready var music_button = $music/button
onready var music_value = $music/value
onready var sfx_button = $sfx/button
onready var sfx_value = $sfx/value
onready var bycicle = $".".find_parent('Control').get_node("Sfx/bycicle")

var count = 0
var interval = 8
var last_time = 0
var step = 3

func _ready():
	music_value.value = Settings.music
	sfx_value.value = Settings.sfx

func _process(_delta):
	count += 1
	if music_button.has_focus():
		if Input.is_action_pressed('ui_right') and count > last_time + interval:
			last_time = count
			music_value.value += step
			Settings.music = music_value.value
			bycicle.play()
		if Input.is_action_pressed('ui_left') and count > last_time + interval:
			last_time = count
			music_value.value -= step
			Settings.music = music_value.value			
			bycicle.play()

	if sfx_button.has_focus():
		if Input.is_action_pressed('ui_right') and count > last_time + interval:
			last_time = count
			sfx_value.value += step
			Settings.sfx = sfx_value.value
			bycicle.play()
		if Input.is_action_pressed('ui_left') and count > last_time + interval:
			last_time = count
			sfx_value.value -= step
			Settings.sfx = sfx_value.value
			bycicle.play()

	if fullscreen_button.has_focus():
		if Input.is_action_just_pressed('toggle'):
			OS.window_fullscreen = !OS.window_fullscreen
	if OS.window_fullscreen == true:
		fullscreen_value.text = 'YES'
	else:
		fullscreen_value.text = 'NO'

func _on_music_value_changed(value):
	AudioServer.set_bus_volume_db(1, value)

func _on_sfx_value_changed(value):
	AudioServer.set_bus_volume_db(2, value)
