extends Control


onready var pause_menu = $Panel
onready var _continue = $Panel/GridContainer/continue
onready var _quit = $Panel/GridContainer/quit
onready var select_sound = $Sfx/select
onready var start_sound = $Sfx/start
onready var pause_sound = $Sfx/short_sweep
onready var panel_mask = $Mask/AnimationPlayer
onready var timer = get_node("Timer")
onready var display_timer = get_node("Display_timer")
onready var timer_label = display_timer.get_node("label")

var count = 0
var interval = 8
var last_time = 0
var opt = 1
var menu_opt_num = 2
var timer_value = 3
var is_pausing = false

func _process(_delta):
	if pause_menu.is_visible_in_tree():
		if Input.is_action_pressed('ui_up') and (count > last_time + interval):
			last_time = count
			opt = Utils._next(opt, menu_opt_num, -1)
			select_sound.play()
		if Input.is_action_pressed('ui_down') and (count > last_time + interval):
			last_time = count
			opt = Utils._next(opt, menu_opt_num, 1)
			select_sound.play()

		count += 1
		Utils.selector(1, opt, _continue)
		Utils.selector(2, opt, _quit)
	
	timer_label.text = str(timer_value)


func _input(event):
	if event.is_action_pressed("pause") and is_pausing == false:
		is_pausing = true
		get_tree().paused = true
		pause_menu.show()
		panel_mask.play("expand")
		pause_sound.play()
		_continue.grab_focus()

func _on_continue_pressed():
	start_sound.play()
	panel_mask.play("reduce")
	yield(get_tree().create_timer(0.3), "timeout")
	pause_menu.hide()
	yield(get_tree().create_timer(0.3), "timeout")	
	timer.set_wait_time(0.5)
	timer.start()
	display_timer.show()


func _on_Timer_timeout():
	timer_value -= 1
	if timer_value == 0:
		is_pausing = false
		get_tree().paused = false
		display_timer.hide()
		timer.stop()
		timer_value = 3
