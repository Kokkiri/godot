extends Node


signal quit
signal replace_main_scene

onready var time_left = $Hud/time_left
onready var	animation = $Animation
onready var	left_button = $Hud/left_button_rect/left_button
onready var right_button = $Hud/right_button_rect/right_button
onready var hud_mask = $Hud/AnimationPlayer
onready var cancel_sound = $Pause/Sfx/cancel
onready var wind_sound = $Sfx/soft_wind
onready var x_scale = 0


var data = Data.data
var current_animation = '1'
var next_animation = '1_2'
var start_animation_duration = ''
var end_animation_duration = ''
var choice = ''
var x_scale_increase = true
var speed_ratio = 3


func _ready():
	get_tree().paused = false	
	animation.play(data[current_animation]["animation"])
	left_button.text = data[current_animation]["text_left"]
	right_button.text = data[current_animation]["text_right"]
	animation.speed_scale = 0.2 * speed_ratio
	time_left.max_value = 12 * 100 / 27 / animation.speed_scale
	time_left.value = 12 * 100 / 27 / animation.speed_scale
	wind_sound.play()
	

func _on_Animation_animation_finished():
	var animation_type = data[current_animation]['type']
	if animation_type == 'end':
		if choice != '':
			next_animation = choice
		else:
			next_animation = '1'
 
		left_button.text = data[next_animation]["text_left"]
		right_button.text = data[next_animation]["text_right"]
		left_button.release_focus()
		right_button.release_focus()

		time_left.max_value = total_animation_duration(next_animation) * 100 / 25 / animation.speed_scale
		time_left.value = time_left.max_value

		choice = ''
		x_scale = 0
		x_scale_increase = true
	else:
		next_animation = data[current_animation]["default_next_screen"]
	animation.play(data[next_animation]["animation"])
	current_animation = next_animation

func _process(_delta):

	if Input.is_action_just_pressed('ui_left'):
		left_button.grab_focus()
		choice = data[current_animation]['left_next_screen']
	if Input.is_action_just_pressed('ui_right'):
		right_button.grab_focus()
		choice = data[current_animation]['right_next_screen']

	if data[current_animation]['type'] == 'end' and animation.frame == animation.frames.get_frame_count(data[current_animation]["animation"]) - 2:
		hud_mask.play("reduce-expand")
	time_left.value -= 1

func total_animation_duration(anim):
	start_animation_duration = animation.frames.get_frame_count(data[anim]["animation"])
	end_animation_duration = animation.frames.get_frame_count(data[anim]["default_next_screen"])
	var res = start_animation_duration + end_animation_duration
	return res

func _on_quit_pressed():
	cancel_sound.play()
	yield(get_tree().create_timer(0.3), "timeout")
	emit_signal("quit")
